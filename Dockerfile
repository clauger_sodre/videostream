FROM node:12
# RUN apt update 
RUN apt-get update 
WORKDIR /usr/src
COPY package*.json ./
RUN npm ci
RUN npm prune
COPY . .
EXPOSE 3000
CMD ["npm", "start"]