const express = require('express');
const app = express();
const fs = require("fs");
const path = require('path')
const router = require("./Routers/router.js")
const util = require('util')
const { pipeline, Readable, Writable } = require('stream');


const port = (process.env.PORT || 8000);

app.listen(port, function () {
    console.log("listen on port " + port)
})


app.get('/', router);

app.get("/video", function (req, res) {
    const range = req.headers.range;
    if (!range) {
        res.status(400).send("Requires Range header");
    }
    let videoName = "/video/Record.mp4"
    const videoPath = path.join(__dirname, videoName)
    let videoSize = 0;
    try {
        ///try to catch a stored videoI
        videoSize = fs.statSync(videoPath).size;
    }
    catch (err) {
        //If can't find a video send a message erro
        console.log(`Cant find file ${videoName} ` + "\n" + err)
        return err
    }

    //Create Range
    const CHUNK_SIZE = 10 ** 6; // 1MB
    const start = Number(range.replace(/\D/g, ""));
    const end = Math.min(start + CHUNK_SIZE, videoSize - 1);

    // Create headers
    const contentLength = end - start + 1;
    const headers = {
        "Content-Range": `bytes ${start}-${end}/${videoSize}`,
        "Accept-Ranges": "bytes",
        "Content-Length": contentLength,
        "Content-Type": "video/mp4",
    };

    // HTTP Status 206 for Partial Content
    res.writeHead(206, headers);

    // create video read stream for this particular chunk
    const videoStream = fs.createReadStream(videoPath, { start, end });

    //Convert a callback function to a async await function
    const pipelineAsync = util.promisify(pipeline)

    
    // Stream the video chunk to the client
    //const writebleStream = videoStream.pipe(res);

    //Created a pipeline because if some erro occurs the videoStream(res) is destroyed
    enviaPipe();
    async function enviaPipe() {
        try {
            await pipelineAsync(
                //where the data come
                videoStream,
                //from the data will be delivery
                await util.promisify(videoStream.pipe(res)),(err)=>{
                    if(err){
                        console.log(err)
                    }
                }
            );
        }
        catch (err) {
            console.log(err)
        }
    };

});