const express = require('express');
var router = express.Router();
const path = require('path')

router.get('/',function (req, res) {
    //find the index page
    let localFolder=__dirname;
    let IndexPage="../pages/index.html";
    const indexPath = path.join(__dirname, IndexPage)
    //send the index page
    res.sendFile(indexPath)
});

module.exports= router;